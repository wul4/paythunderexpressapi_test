package com.paythunder.ptexpressapitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.paythunder.ptexpressapi.PayThunderSDK;

public class MainActivity extends AppCompatActivity {

    protected static final String TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize PayThunderSDK
        PayThunderSDK.init(this);
    }

    public void launchPT(View view){

        if(PayThunderSDK.launch()){
            Log.d(TAG, "PayThunder Express App launch successfully.");
            // do something

        } else {
            Log.d(TAG, "Unable to launch PayThunder Express App.");
            //do something

        }

    }
}
