Sample App using PayThunder Express API for Android
==========================================

[PayThunder](http://www.paythunder.com/) allows mobile payments & marketing as well as additional services combined with merchants applications, or working with the merchant POS.

For illustration purposes, this repository contains a sample project which integrates PayThunder Express API for making PayThunder payments requests.

Integration instructions
------------------------

### Requirements

*   Android SDK version 15 or later.
*   PayThunder Express version 2.3 or later installed
*   Availability of ‘valid’ user credentials by the PayThunder Express app developer. For this
    purpose sign up at PayThunder Express App.

### Setup


##### If you use gradle, then add the following dependency:

```
compile project(':ptexpressapi')
```

### Sample code  (See the Testsampleapp for an example)

Initialize PayThunder by calling PayThunderAccess.init method on ‘onCreate’:

```java
@Override
protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState); [...]
  // Init PayThunder
  PayThunderSDK.init(this);
}
```


Launch PayThunder Express App.

```java
    public void launchPT(View view){

        if(PayThunderSDK.launch()){
            Log.d(TAG, "PayThunder Express App launch successfully.");
            // do something

        } else {
            Log.d(TAG, "Unable to launch PayThunder Express App.");
            //do something

        }

    }
```